package com.davud.teachme.controllers;

import com.davud.teachme.entity.Lesson;
import com.davud.teachme.services.LessonService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LessonsController {
    private LessonService lessonService;

    @Inject
    LessonsController(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    public Single<List<Lesson>> getLessons() {
        return lessonService.getLessons();
    }
}
