package com.davud.teachme.controllers;

import com.davud.teachme.entity.AuthResult;
import com.davud.teachme.entity.User;
import com.davud.teachme.services.AuthService;

import javax.inject.Inject;

import io.reactivex.Single;

public final class LoginController {

    private AuthService authService;

    @Inject
    public LoginController(AuthService authService) {
        this.authService = authService;
    }

    public Single<AuthResult> doLogin(User user) {
        return authService.doLogin(user);
    }
}
