package com.davud.teachme.controllers;

import  android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.davud.teachme.entity.Error;
import com.google.gson.Gson;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class ErrorController {
    private final ConnectivityManager cm;
    private final Gson gson;

    @Inject
    public ErrorController(ConnectivityManager cm, Gson gson) {
        this.cm = cm;
        this.gson = gson;
    }

    public Error convertBody(Throwable e) throws IOException {
        if (isConnected()) {
            ResponseBody body = ((HttpException) e).response().errorBody();
            assert body != null;
            return gson.fromJson(body.string(), Error.class);
        } else return new Error("no network");
    }

    private boolean isConnected() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }
}
