package com.davud.teachme.controllers;

import com.davud.teachme.entity.Profile;
import com.davud.teachme.entity.UserType;
import com.davud.teachme.services.ProfileService;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public class ProfileController {
    private ProfileService profileService;
    @Inject
    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }
    public Single<Profile> getProfile(){
        return profileService.getProfile();
    }
    public Single<UserType> getType(){
        return profileService.getType();
    }
    public Single<Profile> createProfile(Profile profile){return profileService.createProfile(profile);}
    public Single<ResponseBody> updateProfile(Profile profile){return profileService.updateProfile(profile);}
}
