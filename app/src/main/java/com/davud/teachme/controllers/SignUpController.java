package com.davud.teachme.controllers;

import com.davud.teachme.entity.AuthResult;
import com.davud.teachme.entity.User;
import com.davud.teachme.services.AuthService;

import javax.inject.Inject;

import io.reactivex.Single;

public class SignUpController {
    private AuthService authService;

    @Inject
    public SignUpController(AuthService authService) {
        this.authService = authService;
    }
    public Single<AuthResult> signUp(User user){
        return authService.signUp(user);
    }

}
