package com.davud.teachme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.davud.teachme.utils.ItemClickListener;
import com.davud.teachme.utils.ItemLongClickListener;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 4/3/2017.
 */
public abstract class BaseAdapter<E, Holder extends BaseAdapter.Holder<E>> extends RecyclerView.Adapter<Holder> {
    @Getter
    private Context context;
    @Getter @Setter private List<E> items;
    @Getter @Setter private List<E> backup;
    @Getter private ItemClickListener itemClickListener;
    @Getter private ItemLongClickListener longClickListener;

    BaseAdapter(Context context) {
        this.context = context;
        this.items = new ArrayList<>();
        this.backup = new ArrayList<>();
        this.itemClickListener = null;
        this.longClickListener = null;
    }

    public BaseAdapter(Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.items = new ArrayList<>();
        this.backup = new ArrayList<>();
        this.itemClickListener = itemClickListener;
    }


    public BaseAdapter(Context context, ItemClickListener itemClickListener, ItemLongClickListener longClickListener) {
        this.context = context;
        this.items = new ArrayList<>();
        this.backup = new ArrayList<>();
        this.itemClickListener = itemClickListener;
        this.longClickListener = longClickListener;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(getItemAt(position));
    }

    @Override
    public void onViewRecycled(Holder holder) {
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public int getBackupItemsCount() {
        return this.backup.size();
    }

    @NonNull
    public E getItemAt(int position) {
        return this.items.get(position);
    }

    public void removeItemAt(int position) {
        this.items.remove(position);
        setBackup(this.items);
        notifyItemRemoved(position);
    }


    public void clear() {
        this.items.clear();
        setBackup(this.items);
        notifyDataSetChanged();
    }

    public void add(E item) {
        this.items.add(item);
        setBackup(this.items);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addFirst(E item) {
        this.items.add(0, item);
        setBackup(this.items);
        notifyItemInserted(0);
    }

    public void addAll(List<E> items) {
        this.items.clear();
        this.items.addAll(items);
        setBackup(this.items);
        notifyDataSetChanged();
    }

    public void addAllNoClear(List<E> items) {
        this.items.addAll(items);
        setBackup(this.items);
        notifyDataSetChanged();
    }
    public void updateItem(int position, E item) {
        this.items.set(position, item);
        backup = this.items;
        notifyItemChanged(position);
    }

    public void updateVerticalItem(int horizoontal,int vertical) {
//        notifyItemChanged(horizoontal);
    }

    public E getItemAtPosition(int position) {
        return this.items.get(position);
    }

    public void clearAll(List<E> items) {
        this.items.clear();
    }

    static abstract class Holder<E> extends RecyclerView.ViewHolder {
        void bind(E item){};
        abstract void unbind();
        Holder(View itemView) {
            super(itemView);
        }
    }

}
