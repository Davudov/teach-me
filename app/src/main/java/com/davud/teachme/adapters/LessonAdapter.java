package com.davud.teachme.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.davud.teachme.R;
import com.davud.teachme.entity.Lesson;
import com.davud.teachme.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LessonAdapter extends BaseAdapter<Lesson, LessonAdapter.LessonViewHolder> implements Filterable {
    public LessonAdapter(Context context, ItemClickListener itemClickListener) {
        super(context, itemClickListener);
    }

    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.lesson_item, viewGroup, false);
        return new LessonViewHolder(view, getItemClickListener());
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults filterResults = new FilterResults();
            final String query = constraint.toString().toLowerCase();
            if(query.isEmpty()) {
                filterResults.count = getBackup().size();
                filterResults.values = getBackup();
            }else {
                List<Lesson> filtered = new ArrayList<>();
                for(Lesson lesson : getBackup()) {
                    if(lesson.getCreator().getProfile().getFirstname().toLowerCase().startsWith(query)) {
                        filtered.add(lesson);
                    }
                }
                filterResults.count = filtered.size();
                filterResults.values = filtered;
            }
            return filterResults;
        }

        @SuppressWarnings({"UNCHECKED_CAST", "unchecked"})
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            setItems((List<Lesson>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() {
        return filter;
    }


    class LessonViewHolder extends BaseAdapter.Holder<Lesson> implements View.OnClickListener {
        private final ItemClickListener itemClickListener;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.location)
        TextView location;

        @Override
        void unbind() {

        }

        @Override
        void bind(Lesson item) {
            super.bind(item);
            String fullName;
            if (item.getCreator().getProfile().getLastname() != null) {
                fullName = item.getCreator().getProfile().getFirstname() + " " + item.getCreator().getProfile().getLastname();
            } else
                fullName = item.getCreator().getProfile().getFirstname();

            name.setText(fullName);
            if (item.getCreator().getProfile().getAddress() != null)
            location.setText(item.getCreator().getProfile().getAddress());
        }

        LessonViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {

        }
    }
}
