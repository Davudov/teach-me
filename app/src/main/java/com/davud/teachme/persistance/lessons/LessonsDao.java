package com.davud.teachme.persistance.lessons;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.davud.teachme.entity.Lesson;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface LessonsDao {
    @Query("select * from lessons")
    Single<List<Lesson>> getLessons();

    @Insert
    void saveLessons(List<Lesson> lessons);

    @Query("delete from lessons")
    void deleteLessons();

    @Transaction
    default void updateLessons(List<Lesson> lessons){
        deleteLessons();
        saveLessons(lessons);
    }
}
