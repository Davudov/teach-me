package com.davud.teachme.persistance.profile;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.davud.teachme.entity.Profile;

import io.reactivex.Single;

@Dao
public interface ProfileDao {
    @Query("select * from profile")
    Single<Profile> getProfile();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProfile(Profile profile);

    @Query("delete from profile")
    void deleteProfile();

    @Transaction
    default void updateProfile(Profile profile) {
        deleteProfile();
        saveProfile(profile);
    }
}
