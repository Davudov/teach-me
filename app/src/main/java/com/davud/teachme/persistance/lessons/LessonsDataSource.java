package com.davud.teachme.persistance.lessons;

import com.davud.teachme.entity.Lesson;

import java.util.List;

import io.reactivex.Single;

interface LessonsDataSource {
    Single<List<Lesson>> getLessons();
    void saveLessons(List<Lesson> lessons);
    void deleteLessons();
    void updateLessons(List<Lesson> lessons);
}
