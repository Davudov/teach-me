package com.davud.teachme.persistance.profile;

import com.davud.teachme.entity.Profile;

import io.reactivex.Single;

public interface ProfileDataSource {
    Single<Profile> getProfile();

    void saveProfile(Profile profile);

    void deleteProfile();

    void updateProfile(Profile profile);
}
