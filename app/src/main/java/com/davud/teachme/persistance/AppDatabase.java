package com.davud.teachme.persistance;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.davud.teachme.entity.Lesson;
import com.davud.teachme.entity.Profile;
import com.davud.teachme.persistance.lessons.LessonsDao;
import com.davud.teachme.persistance.profile.ProfileDao;
import com.davud.teachme.utils.Converters;

@Database(entities = {Profile.class, Lesson.class},version = 3, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public static String name = "database";
    public abstract ProfileDao userDao();
    public abstract LessonsDao lessonsDao();
}
