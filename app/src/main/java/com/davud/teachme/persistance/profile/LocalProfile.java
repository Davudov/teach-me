package com.davud.teachme.persistance.profile;

import com.davud.teachme.entity.Profile;

import javax.inject.Inject;

import io.reactivex.Single;

public class LocalProfile implements ProfileDataSource {
    private final ProfileDao profileDao;
    @Inject
    public LocalProfile(ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    @Override
    public Single<Profile> getProfile() {
        return profileDao.getProfile();
    }

    @Override
    public void saveProfile(Profile profile) {
        profileDao.saveProfile(profile);
    }

    @Override
    public void deleteProfile() {
        profileDao.deleteProfile();
    }

    @Override
    public void updateProfile(Profile p) {
        profileDao.updateProfile(p);
    }
}
