package com.davud.teachme.persistance.lessons;

import com.davud.teachme.entity.Lesson;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LocalLessons implements LessonsDataSource {
    private final LessonsDao lessonsDao;
    @Inject
    public LocalLessons(LessonsDao lessonsDao) {
        this.lessonsDao = lessonsDao;
    }

    @Override
    public Single<List<Lesson>> getLessons() {
        return lessonsDao.getLessons();
    }

    @Override
    public void saveLessons(List<Lesson> lessons) {
        lessonsDao.saveLessons(lessons);
    }

    @Override
    public void deleteLessons() {
        lessonsDao.deleteLessons();
    }

    @Override
    public void updateLessons(List<Lesson> lessons) {
        lessonsDao.updateLessons(lessons);
    }
}
