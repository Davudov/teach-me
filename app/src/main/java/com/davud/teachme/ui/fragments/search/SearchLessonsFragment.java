package com.davud.teachme.ui.fragments.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseFragment;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.adapters.LessonAdapter;
import com.davud.teachme.controllers.LessonsController;
import com.davud.teachme.entity.Lesson;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.presentation.search.SearchLessonsView;
import com.davud.teachme.presentation.search.SearchPresenter;
import com.davud.teachme.utils.ItemClickListener;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;

public class SearchLessonsFragment extends BaseFragment implements SearchLessonsView, ItemClickListener, SearchView.OnQueryTextListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout mSwipe;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @Inject
    LocalLessons localLessons;
    @Inject
    LessonsController lessonsController;
    @InjectPresenter
    SearchPresenter presenter;

    @ProvidePresenter
    SearchPresenter providePresenter() {
        return new SearchPresenter(localLessons, lessonsController);
    }

    LessonAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getStaticComponent(Objects.requireNonNull(getContext())).inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void viewCreated(@Nullable Bundle savedInstanceState) {
        adapter = new LessonAdapter(getContext(), this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        toolbar.setTitle("Search");
        toolbar.inflateMenu(R.menu.search);
        final MenuItem item = toolbar.getMenu().findItem(R.id.search_menu);
        final MenuItem update = toolbar.getMenu().findItem(R.id.update);
        update.setOnMenuItemClickListener(menuItem -> {
            presenter.getLessons();
            return true;
        });
        final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        mSwipe.setOnRefreshListener(() -> presenter.getLessons());
    }

    @Override
    public void onItemClick(int position, View view) {

    }

    @Override
    public void onReadyData(List<Lesson> lessons) {
        adapter.addAll(lessons);
    }

    @Override
    public void showLoader() {
        mSwipe.setRefreshing(true);
    }

    @Override
    public void hideLoader() {
        mSwipe.setRefreshing(false);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return false;
    }
}
