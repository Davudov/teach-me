package com.davud.teachme.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseActivity;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.LoginController;
import com.davud.teachme.entity.User;
import com.davud.teachme.presentation.login.LoginPresenter;
import com.davud.teachme.presentation.login.LoginView;
import com.davud.teachme.utils.UserUtil;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.auth_login)
    TextInputEditText mAuthLogin;
    @BindView(R.id.auth_password)
    TextInputEditText mAuthPassword;
    @BindView(R.id.auth_login_wrapper)
    TextInputLayout mAuthLoginWrapper;
    @BindView(R.id.auth_password_wrapper)
    TextInputLayout mAuthPasswordWrapper;
    @BindView(R.id.authenticate)
    Button mAuthButton;
    @BindView(R.id.create_account)
    TextView signUp;
    private ProgressDialog progressDialog;
    @Inject
    LoginController loginController;
    @Inject
    ErrorController errorConverter;
    @Inject
    UserUtil userUtil;
    @InjectPresenter
    LoginPresenter loginPresenter;

    @ProvidePresenter
    LoginPresenter loginPresenter() {
        return new LoginPresenter(loginController, userUtil, errorConverter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getStaticComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.login_auth_process));
    }


    public void onSubmitClick(View view) {

        final String login = Objects.requireNonNull(mAuthLogin.getText()).toString();
        final String password = Objects.requireNonNull(mAuthPassword.getText()).toString();

        if (TextUtils.isEmpty(login)) {
            mAuthLoginWrapper.setErrorEnabled(true);
            mAuthLoginWrapper.setError(getString(R.string.login_username_empty));
            return;
        } else {
            mAuthLoginWrapper.setErrorEnabled(false);
            mAuthLoginWrapper.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            mAuthPasswordWrapper.setErrorEnabled(true);
            mAuthPasswordWrapper.setError(getString(R.string.login_password_empty));
            return;
        } else {
            mAuthPasswordWrapper.setErrorEnabled(false);
            mAuthPasswordWrapper.setError(null);
        }

        loginPresenter.doLogin(new User(login, password));

    }


    public final void goSignUp(View view) {
        startActivity(new Intent(this, SelectActivity.class));
        finish();
    }

    public void startProgress() {
        progressDialog.show();
    }

    public void stopProgress() {
        progressDialog.dismiss();
    }

    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void onSuccess() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }
}
