package com.davud.teachme.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseActivity;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.controllers.LessonsController;
import com.davud.teachme.controllers.ProfileController;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;
import com.davud.teachme.presentation.main.MainPresenter;
import com.davud.teachme.presentation.main.MainView;
import com.davud.teachme.ui.fragments.profile.ProfileStudent;
import com.davud.teachme.ui.fragments.profile.ProfileTeacher;
import com.davud.teachme.ui.fragments.saved.SavedFragment;
import com.davud.teachme.ui.fragments.search.SearchLessonsFragment;
import com.davud.teachme.utils.Constants;
import com.davud.teachme.utils.Screens;
import com.davud.teachme.utils.UserUtil;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements MainView, TabLayout.OnTabSelectedListener {
    @Inject
    ProfileController profileController;
    @Inject
    UserUtil userUtil;
    @Inject
    LocalProfile localProfile;
    @Inject
    LocalLessons localLessons;
    @Inject
    LessonsController lessonsController;

    @ProvidePresenter
    MainPresenter providePresenter() {
        return new MainPresenter(profileController, localProfile, userUtil, localLessons, lessonsController);
    }

    @InjectPresenter
    MainPresenter presenter;
    @BindView(R.id.placeholder)
    FrameLayout placeholder;
    @BindView(R.id.root)
    CoordinatorLayout mRoot;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.progress_circular)
    ProgressBar progressBar;
    @Inject
    Router router;
    @Inject
    NavigatorHolder navigatorHolder;
    Bundle instances;
    private long backPressed = 0L;
    private final Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.placeholder) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return MainActivity.this.createFragment(screenKey);
        }

        @Override
        protected void showSystemMessage(String message) {
            Snackbar.make(placeholder, message, Snackbar.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }
    };


    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getStaticComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.instances = savedInstanceState;
        tabLayout.addOnTabSelectedListener(this);
    }

    Fragment createFragment(String screenKey) {
        if (screenKey.equals(Screens.SEARCH))
            return new SearchLessonsFragment();
        if (screenKey.equals(Screens.PROFILE_STUDENT))
            return new ProfileStudent();
        if (screenKey.equals(Screens.PROFILE_TEACHER))
            return new ProfileTeacher();
        if (screenKey.equals(Screens.SAVED))
            return new SavedFragment();
        return new SearchLessonsFragment();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getCurrentFragment();
        if (isRootFragment(currentFragment)) {
            if (System.currentTimeMillis() - backPressed > 2000) {
                backPressed = System.currentTimeMillis();
                showSnack();
            } else
                super.onBackPressed();
        } else super.onBackPressed();

    }

    private void showSnack() {
        Snackbar.make(mRoot, getString(R.string.press_again_to_exit), Snackbar.LENGTH_SHORT).show();
    }

    private boolean isRootFragment(Fragment currentFragment) {
        if (currentFragment instanceof SearchLessonsFragment)
            return true;
        else if (currentFragment instanceof ProfileStudent)
            return true;
        else if (currentFragment instanceof ProfileTeacher)
            return true;
        else return currentFragment instanceof SavedFragment;
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.placeholder);
    }

    @Override
    public void onTeacherType() {
        Timber.d("Teacher type");
        //todo Create orientation responsible fragments
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_star).setContentDescription("teacher saved"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.person_icon_black).setContentDescription("teacher profile"));
        Objects.requireNonNull(tabLayout.getTabAt(1)).select();
//        if (instances == null) {
//            navigator.applyCommands(new Command[]{new Replace(Screens.PROFILE, 1)});
//        }
//        bottomNavigation.inflateMenu(R.menu.bottom_navigation_teacher);
//        bottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public void onStudentType() {
        Timber.d("Student type");
//        todo Create orientation responsible fragments
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_star).setContentDescription("student saved"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_search).setContentDescription("student search"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.person_icon_black).setContentDescription("student profile"));
        Objects.requireNonNull(tabLayout.getTabAt(1)).select();
//        if (instances == null) {
//            navigator.applyCommands(new Command[]{new Replace(Screens.SEARCH, 1)});
//        }
//        bottomNavigation.inflateMenu(R.menu.bottom_navigation_student);
//        bottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void getProfileType(String userType) {
        Timber.d("userType %s", userType);
        if (userType.equals(Constants.TYPE_TEACHER)) {
            onTeacherType();
        } else if (userType.equals(Constants.TYPE_STUDENT)) {
            onStudentType();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (Objects.requireNonNull(tab.getContentDescription()).toString().contains("teacher")) {
            switch (tab.getPosition()) {
                case 0: {
                    router.replaceScreen(Screens.SAVED);
                    break;
                }
                case 1: {
                    router.replaceScreen(Screens.PROFILE_TEACHER);
                    break;
                }
            }
        } else if (tab.getContentDescription().toString().contains("student")) {
            switch (tab.getPosition()) {
                case 0: {
                    router.replaceScreen(Screens.SAVED);
                    break;
                }
                case 1: {
                    router.replaceScreen(Screens.SEARCH);
                    break;
                }
                case 2: {
                    router.replaceScreen(Screens.PROFILE_STUDENT);
                    break;
                }

            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
