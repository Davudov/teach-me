package com.davud.teachme.ui.fragments.map;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.davud.teachme.BaseFragment;
import com.davud.teachme.R;

public class PickLocationFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_pick_location;
    }

    @Override
    protected void viewCreated(@Nullable Bundle savedInstanceState) {

    }
}
