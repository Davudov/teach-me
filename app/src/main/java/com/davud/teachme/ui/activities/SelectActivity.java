package com.davud.teachme.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.davud.teachme.BaseActivity;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.utils.Constants;
import com.davud.teachme.utils.UserUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.teacher)
    TextView teacher;

    @BindView(R.id.student)
    TextView student;

    @Inject
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getStaticComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        ButterKnife.bind(this);
        student.setOnClickListener(this);
        teacher.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.teacher: {
                sharedPreferences.edit().putString(UserUtil.USER_TYPE,Constants.TYPE_TEACHER).apply();
                startActivity(new Intent(SelectActivity.this, RegisterActivity.class));
                finish();
                break;
            }
            case R.id.student: {
                sharedPreferences.edit().putString(UserUtil.USER_TYPE,Constants.TYPE_STUDENT).apply();
                startActivity(new Intent(SelectActivity.this, RegisterActivity.class));
                finish();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SelectActivity.this,LoginActivity.class));
        finish();
    }
}
