package com.davud.teachme.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.davud.teachme.BaseActivity;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.utils.UserUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {
    Handler mHandler = new Handler();
    @Inject
    UserUtil userUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getStaticComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(splashRunnable, 1500);
    }

    Runnable splashRunnable = () -> {
        if (userUtil.isUserLoggedIn()){
            startActivity(MainActivity.getStartIntent(SplashActivity.this));
            finish();
        }
        else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null)
            mHandler.removeCallbacks(splashRunnable);
    }
}
