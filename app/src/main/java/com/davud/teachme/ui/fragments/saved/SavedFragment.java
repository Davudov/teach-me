package com.davud.teachme.ui.fragments.saved;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.davud.teachme.BaseFragment;
import com.davud.teachme.R;

public class SavedFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_saved;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    protected void viewCreated(@Nullable Bundle savedInstanceState) {

    }
}
