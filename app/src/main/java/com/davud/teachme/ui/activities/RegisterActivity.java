package com.davud.teachme.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseActivity;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.SignUpController;
import com.davud.teachme.entity.User;
import com.davud.teachme.presentation.register.RegisterPresenter;
import com.davud.teachme.presentation.register.RegisterView;
import com.davud.teachme.utils.UserUtil;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity implements RegisterView, TextWatcher {
    @Inject
    UserUtil userUtil;
    @Inject
    SignUpController signUpController;
    @Inject
    SharedPreferences preferences;
    @Inject
    ErrorController errorController;
    @BindView(R.id.auth_password)
    TextInputEditText pass;
    @BindView(R.id.sign_password_wrapper)
    TextInputLayout passWrapper;
    @BindView(R.id.auth_password2)
    TextInputEditText pass2;
    @BindView(R.id.sign_password2_wrapper)
    TextInputLayout pass2Wrapper;
    @BindView(R.id.auth_username)
    TextInputEditText mUserName;
    @BindView(R.id.sign_username_wrapper)
    TextInputLayout userNameWrapper;
    @BindView(R.id.auth_email)
    TextInputEditText mRegisterEmail;
    @BindView(R.id.sign_up_email_wrapper)
    TextInputLayout emailWrapper;
    @BindView(R.id.sign_up)
    Button signUpBtn;
    @InjectPresenter
    RegisterPresenter presenter;
    ProgressDialog progressDialog;
    String type = null;
    @ProvidePresenter
    RegisterPresenter registerPresenter() {
        return new RegisterPresenter(userUtil,signUpController, errorController, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getStaticComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.sign_up_dialog));
        type = preferences.getString(UserUtil.USER_TYPE,null);
        pass2.addTextChangedListener(this);
    }


    public void onSubmitRegister(View view) {
        final String email = Objects.requireNonNull(mRegisterEmail.getText()).toString();
        final String password = Objects.requireNonNull(pass.getText()).toString();
        final String password2 = Objects.requireNonNull(pass2.getText()).toString();
        final String userName = Objects.requireNonNull(mUserName.getText()).toString();
        if (TextUtils.isEmpty(email)) {
            emailWrapper.setErrorEnabled(true);
            emailWrapper.setError(getString(R.string.login_username_empty));
            return;
        } else {
            emailWrapper.setErrorEnabled(false);
            emailWrapper.setError(null);
        }
        if (TextUtils.isEmpty(userName)) {
            userNameWrapper.setErrorEnabled(true);
            userNameWrapper.setError(getString(R.string.login_password_empty));
            return;
        } else {
            userNameWrapper.setErrorEnabled(false);
            userNameWrapper.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            passWrapper.setErrorEnabled(true);
            passWrapper.setError(getString(R.string.login_password_empty));
            return;
        } else {
            passWrapper.setErrorEnabled(false);
            passWrapper.setError(null);
        }

        if (TextUtils.isEmpty(password2)) {
            pass2Wrapper.setErrorEnabled(true);
            pass2Wrapper.setError(getString(R.string.login_password_empty));
            return;
        } else {
            pass2Wrapper.setErrorEnabled(false);
            pass2Wrapper.setError(null);
        }
        if (!TextUtils.equals(pass.getText(),pass2.getText())){
            pass2Wrapper.setErrorEnabled(true);
            pass2Wrapper.setError(getString(R.string.sign_up_pass_not_match));
            return;
        } else {
            pass2Wrapper.setErrorEnabled(false);
            pass2Wrapper.setError(null);
        }
        presenter.signUp(new User(email,userName,password,password2,type));
    }
    @Override
    public void startProgress() {
        progressDialog.show();
    }

    @Override
    public void stopProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!TextUtils.equals(pass.getText(),pass2.getText())){
            pass2Wrapper.setErrorEnabled(true);
            pass2Wrapper.setError(getString(R.string.sign_up_pass_not_match));
        } else {
            pass2Wrapper.setErrorEnabled(false);
            pass2Wrapper.setError(null);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this,SelectActivity.class));
        finish();
    }
}
