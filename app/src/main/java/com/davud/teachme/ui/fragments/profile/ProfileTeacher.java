package com.davud.teachme.ui.fragments.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseFragment;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.ProfileController;
import com.davud.teachme.entity.Profile;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;
import com.davud.teachme.presentation.teacher_profile.TeacherProfilePresenter;
import com.davud.teachme.presentation.teacher_profile.TeacherProfileView;
import com.davud.teachme.ui.activities.LoginActivity;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;

public class ProfileTeacher extends BaseFragment implements TeacherProfileView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progress_circular)
    ProgressBar progressBar;
    @BindView(R.id.profile_teacher_name)
    TextView firstName;
    @BindView(R.id.profile_teacher_surname)
    TextView lastName;
//    @BindView(R.id.profile_teacher_bio)
//    TextView bio;
//    @BindView(R.id.profile_teacher_job_position)
//    TextView jobPosition;
    @BindView(R.id.profile_teacher_location)
    TextView location;
    @BindView(R.id.profile_teacher_email)
    TextView email;
    @BindView(R.id.profile_teacher_phone)
    TextView phone;
//    @BindView(R.id.profile_teacher_category)
//    TextView category;
//    @BindView(R.id.profile_teacher_pass)
//    TextView pass;
    MenuItem updateText;
    @Inject
    LocalProfile localProfile;
    @Inject
    ProfileController profileController;
    @Inject
    ErrorController errorController;
    @InjectPresenter
    TeacherProfilePresenter presenter;
    @Inject
    LocalLessons localLessons;

    @ProvidePresenter
    TeacherProfilePresenter providePresenter() {
        return new TeacherProfilePresenter(localProfile, profileController, localLessons);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile_teacher;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getStaticComponent(Objects.requireNonNull(getContext())).inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void viewCreated(@Nullable Bundle savedInstanceState) {
        toolbar.setTitle("Profile");
        toolbar.inflateMenu(R.menu.profile_teacher);
        updateText = toolbar.getMenu().findItem(R.id.update);
        MenuItem signOut = toolbar.getMenu().findItem(R.id.sign_out);
        signOut.setOnMenuItemClickListener(t -> {
            presenter.signOut(getContext());
            startActivity(new Intent(getActivity(), LoginActivity.class));
            Objects.requireNonNull(getActivity()).finish();
            return true;
        });

    }

    @Override
    public void getProfile(Profile profile) {
        setData(profile);
        updateText.setOnMenuItemClickListener(t -> {
            presenter.updateProfile(getData());
            return true;
        });
    }

    @Override
    public void onProfileNotExists() {
        updateText.setTitle(R.string.profile_create);
        updateText.setOnMenuItemClickListener(t -> {
            presenter.createProfile(getData());
            return true;
        });
    }

    @Override
    public void message(String t) {
        Toast.makeText(getContext(), t, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
        updateText.setEnabled(false);
    }

    @Override
    public void stopProgress() {
        progressBar.setVisibility(View.GONE);
        updateText.setEnabled(true);
    }

    private Profile getData() {
        Profile profile = new Profile();
        profile.setFirstname(firstName.getText().toString());
        profile.setLastname(lastName.getText().toString());
        profile.setEmail(email.getText().toString());
        profile.setAddress(location.getText().toString());
        profile.setPhone(phone.getText().toString());
        return profile;
    }

    private void setData(Profile profile) {
        updateText.setTitle(R.string.profile_update);
        firstName.setText(profile.getFirstname());
        lastName.setText(profile.getLastname());
        email.setText(profile.getEmail());
        location.setText(profile.getAddress());
        phone.setText(profile.getPhone());
//        jobPosition.setText(profile);
    }

}
