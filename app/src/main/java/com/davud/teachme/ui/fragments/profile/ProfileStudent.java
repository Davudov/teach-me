package com.davud.teachme.ui.fragments.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.teachme.BaseFragment;
import com.davud.teachme.MyApplication;
import com.davud.teachme.R;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.ProfileController;
import com.davud.teachme.entity.Profile;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;
import com.davud.teachme.presentation.student_profile.StudentProfilePresenter;
import com.davud.teachme.presentation.student_profile.StudentProfileView;
import com.davud.teachme.ui.activities.LoginActivity;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;

public class ProfileStudent extends BaseFragment implements StudentProfileView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    MenuItem updateText;
    @BindView(R.id.profile_student_name)
    EditText firstName;
    @BindView(R.id.profile_student_surname)
    EditText lastName;
    @BindView(R.id.email_student)
    EditText email;
    @BindView(R.id.profile_student_phone)
    EditText phone;
    @BindView(R.id.profile_student_location)
    EditText location;
    @Inject
    ProfileController profileController;
    @Inject
    LocalProfile localProfile;
    @Inject
    ErrorController errorController;
    @Inject
    LocalLessons localLessons;
    @InjectPresenter
    StudentProfilePresenter presenter;
    @ProvidePresenter
    StudentProfilePresenter getPresenter() {
        return new StudentProfilePresenter(profileController, localProfile, localLessons);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile_student;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        (MyApplication.getStaticComponent(Objects.requireNonNull(getContext()))).inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void viewCreated(@Nullable Bundle savedInstanceState) {
        toolbar.setTitle("Profile");
        toolbar.inflateMenu(R.menu.profile_student);
        updateText = toolbar.getMenu().findItem(R.id.update);
        MenuItem signOut = toolbar.getMenu().findItem(R.id.sign_out);
        signOut.setOnMenuItemClickListener(t -> {
            presenter.signOut(getContext());
            startActivity(new Intent(getActivity(), LoginActivity.class));
            Objects.requireNonNull(getActivity()).finish();
            return true;
        });
    }

    private Profile getData() {
        Profile profile = new Profile();
        profile.setFirstname(firstName.getText().toString());
        profile.setLastname(lastName.getText().toString());
        profile.setEmail(email.getText().toString());
        profile.setAddress(location.getText().toString());
        profile.setPhone(phone.getText().toString());
        return profile;
    }

    private void setData(Profile profile) {
        updateText.setTitle(R.string.profile_update);
        firstName.setText(profile.getFirstname());
        lastName.setText(profile.getLastname());
        email.setText(profile.getEmail());
        location.setText(profile.getAddress());
        phone.setText(profile.getPhone());
    }


    @Override
    public void getProfile(Profile profile) {
        setData(profile);
        updateText.setOnMenuItemClickListener(t -> {
            presenter.updateProfile(getData());
            return true;
        });
    }

    @Override
    public void onProfileNotExists() {
        updateText.setTitle(R.string.profile_create);
        updateText.setOnMenuItemClickListener(t -> {
            presenter.createProfile(getData());
            return true;
        });
    }

    @Override
    public void message(String t) {
        Toast.makeText(getContext(), t, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
        updateText.setEnabled(false);
    }

    @Override
    public void stopProgress() {
        progressBar.setVisibility(View.GONE);
        updateText.setEnabled(true);
    }
}
