package com.davud.teachme;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.davud.teachme.dagger.components.AppComponent;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import lombok.Getter;

public abstract class BaseFragment extends MvpAppCompatFragment {
    @Getter
    private AppComponent appComponent;
    private Unbinder unbinder = Unbinder.EMPTY;

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void viewCreated(@Nullable Bundle savedInstanceState);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appComponent = ((MyApplication) Objects.requireNonNull(getActivity()).getApplication()).getComponent();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
