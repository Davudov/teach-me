package com.davud.teachme.custom;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.davud.teachme.utils.UserUtil;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

public class ReceivedCookiesInterceptor implements Interceptor {

    private SharedPreferences preferences;

    public ReceivedCookiesInterceptor(Context context)
    {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        Timber.d("Requested url: %s", request.url().toString());
        builder.addHeader("User-Agent", "Android OS");
        builder.addHeader("Accept", "application/json");
        final String storedCookie = getCookie();
        if (storedCookie != null)
        {
            builder.addHeader(UserUtil.TOKEN, storedCookie);
        }
        Response response = chain.proceed(builder.build());

        return response;
    }
    private String getCookie()
    {
        return preferences.getString(UserUtil.TOKEN, null);
    }

}
