package com.davud.teachme.dagger.modules;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.PowerManager;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicesModule {
    @Provides
    LocationManager locationManager(Application application) {
        return (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
    }

    @Provides
    ConnectivityManager connectivityManager(Application application) {
        return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    PowerManager powerManager(Application application) {
        return (PowerManager) application.getSystemService(Context.POWER_SERVICE);
    }

    @Provides
    NotificationManager notificationsService(Application application) {
        return (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
    }
}
