package com.davud.teachme.dagger.components;

import com.davud.teachme.MyApplication;
import com.davud.teachme.dagger.modules.AppModule;
import com.davud.teachme.dagger.modules.DatabaseModule;
import com.davud.teachme.dagger.modules.NavigationModule;
import com.davud.teachme.dagger.modules.NetworkModule;
import com.davud.teachme.dagger.modules.RetrofitModule;
import com.davud.teachme.dagger.modules.ServicesModule;
import com.davud.teachme.ui.activities.LoginActivity;
import com.davud.teachme.ui.activities.MainActivity;
import com.davud.teachme.ui.activities.RegisterActivity;
import com.davud.teachme.ui.activities.SelectActivity;
import com.davud.teachme.ui.activities.SplashActivity;
import com.davud.teachme.ui.fragments.profile.ProfileStudent;
import com.davud.teachme.ui.fragments.profile.ProfileTeacher;
import com.davud.teachme.ui.fragments.search.SearchLessonsFragment;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, NavigationModule.class,
        RetrofitModule.class, ServicesModule.class, DatabaseModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);
    void inject(SelectActivity selectActivity);
    void inject(MyApplication myApplication);
    void inject(RegisterActivity registerActivity);
    void inject(SplashActivity splashActivity);
    void inject(SearchLessonsFragment searchFragment);
    void inject(LoginActivity loginActivity);
    void inject(ProfileStudent profileStudent);
    void inject(ProfileTeacher profileTeacher);
}
