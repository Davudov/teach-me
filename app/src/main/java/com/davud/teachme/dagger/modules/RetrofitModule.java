package com.davud.teachme.dagger.modules;

import com.davud.teachme.services.AuthService;
import com.davud.teachme.services.LessonService;
import com.davud.teachme.services.ProfileService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RetrofitModule {
    @Provides
    AuthService provideAuthService(@Named("teach_me") Retrofit retrofit) {
        return retrofit.create(AuthService.class);
    }

    @Provides
    LessonService provideLessonService(@Named("teach_me") Retrofit retrofit) {
        return retrofit.create(LessonService.class);
    }

    @Provides
    ProfileService provideProfileService(@Named("teach_me") Retrofit retrofit){
        return retrofit.create(ProfileService.class);
    }
}
