package com.davud.teachme.dagger.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.davud.teachme.persistance.AppDatabase;
import com.davud.teachme.persistance.lessons.LessonsDao;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;
import com.davud.teachme.persistance.profile.ProfileDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
 public class DatabaseModule {
    @Provides
    @Singleton
    AppDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, AppDatabase.name)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
     ProfileDao provideProfileDao(AppDatabase appDatabase) {
        return appDatabase.userDao();
    }

    @Provides
    @Singleton
    LocalProfile provideProfileRep(ProfileDao profileDao) {
        return new LocalProfile(profileDao);
    }

    @Provides
    @Singleton
    LessonsDao provideLessonsDao(AppDatabase appDatabase) {
        return appDatabase.lessonsDao();
    }

    @Provides
    @Singleton
    LocalLessons provideLessonRep(LessonsDao lessonsDao) {
        return new LocalLessons(lessonsDao);
    }
}
