package com.davud.teachme;

import android.app.Application;
import android.content.Context;

import com.davud.teachme.dagger.components.AppComponent;
import com.davud.teachme.dagger.components.DaggerAppComponent;
import com.davud.teachme.dagger.modules.AppModule;
import com.davud.teachme.dagger.modules.NetworkModule;

import timber.log.Timber;

public class MyApplication extends Application {
    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
        initTimber();
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
        appComponent.inject(this);
    }

    public AppComponent getComponent()
    {
        return appComponent;
    }

    public static AppComponent getStaticComponent(Context context)
    {
        return ((MyApplication) context.getApplicationContext()).getComponent();
    }
}
