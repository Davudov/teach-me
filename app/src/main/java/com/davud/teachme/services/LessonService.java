package com.davud.teachme.services;

import com.davud.teachme.entity.Lesson;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface LessonService {
    @GET("lesson/all")
    Single<List<Lesson>> getLessons();
}
