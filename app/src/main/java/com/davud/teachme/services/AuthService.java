package com.davud.teachme.services;

import com.davud.teachme.entity.AuthResult;
import com.davud.teachme.entity.User;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthService {
    @POST("auth/signin")
    Single<AuthResult> doLogin(@Body User user);
    @POST("auth/signup")
    Single<AuthResult> signUp(@Body User user);

}
