package com.davud.teachme.services;

import com.davud.teachme.entity.Profile;
import com.davud.teachme.entity.UserType;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ProfileService {
    @GET("profile/current")
    Single<Profile> getProfile();
    @GET("user/type")
    Single<UserType> getType();
    @POST("profile/create")
    Single<Profile> createProfile(@Body Profile p);
    @PUT("profile/update")
    Single<ResponseBody> updateProfile(@Body Profile p);
}
