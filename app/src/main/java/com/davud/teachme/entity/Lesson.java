package com.davud.teachme.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.RoomWarnings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(tableName = "lessons")
public class Lesson {
    @SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
    @PrimaryKey(autoGenerate = true)
    private int id;
    @Expose
    @SerializedName("__v")
    @ColumnInfo(name = "__vLesson")
    private int __v;
    @Expose
    @SerializedName("price")
    @ColumnInfo(name = "priceLesson")
    private int price;
    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "nameLesson")
    private String name;
    @Expose
    @SerializedName("creator")
    @Embedded
    private Creator creator;
    @Expose
    @SerializedName("_id")
    @ColumnInfo(name = "_idLesson")
    private String _id;
    @Expose
    @SerializedName("category")
    @ColumnInfo(name = "categoryLesson")
    private List<String> category;

}
