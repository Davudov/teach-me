package com.davud.teachme.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class UserType {
    @SerializedName("type")
    @Expose
    private String type;

    public UserType(String type) {
        this.type = type;
    }
}
