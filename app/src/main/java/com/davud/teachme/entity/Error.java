package com.davud.teachme.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class Error {
    @Expose
    @SerializedName("message")
    private String message;

    public Error(String message) {
        this.message = message;
    }
}
