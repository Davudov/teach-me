package com.davud.teachme.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Creator {
    @Expose
    @SerializedName("profile")
    @Embedded
    private Profile profile;
    @Expose
    @SerializedName("__v")
    @ColumnInfo(name = "__vCreator")
    private int __v;
    @Expose
    @SerializedName("type")
    @ColumnInfo(name = "typeType")
    private String type;
    @Expose
    @SerializedName("_id")
    @ColumnInfo(name = "_idCreator")
    private String _id;
    @Expose
    @SerializedName("role")
    @ColumnInfo(name = "roleCreator")
    private String role;
}
