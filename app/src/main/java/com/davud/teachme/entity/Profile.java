package com.davud.teachme.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "profile")
@Getter
@Setter
public class Profile {
    @PrimaryKey()
    @ColumnInfo(name = "db_id")
    private int db_id;
    @Expose
    @SerializedName("__v")
    @ColumnInfo(name = "__v")
    private int __v;
    @Expose
    @SerializedName("firstname")
    @ColumnInfo(name = "firstname")
    private String firstname;
    @Expose
    @SerializedName("userId")
    @ColumnInfo(name = "userId")
    private String userId;
    @Expose
    @SerializedName("_id")
    private String _id;
    @Expose
    @SerializedName("category")
    @ColumnInfo(name = "category")
    private List<String> category;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("lastname")
    @Expose
    String lastname;

    @SerializedName("dob")
    @Expose
    String birthDate;

    @SerializedName("phone")
    @Expose
    String phone;

    @SerializedName("address")
    @Expose
    String address;

    @SerializedName("email")
    @Expose
    String email;

}
