package com.davud.teachme.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AuthResult {

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("success")
    private boolean success;

}
