package com.davud.teachme;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.davud.teachme.dagger.components.AppComponent;

public class BaseActivity extends MvpAppCompatActivity {
    private AppComponent appComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appComponent = ((MyApplication) getApplication()).getComponent();
    }

    protected AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
