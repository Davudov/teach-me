package com.davud.teachme.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.auth0.android.jwt.JWT;

import javax.inject.Inject;

public class UserUtil {
    public static final String TOKEN = "Authorization";
    public static final String USER_TYPE = "USER_TYPE";

    private final SharedPreferences authPreferences;

    public boolean isUserLoggedIn() {
        return getUserCookie() != null;
    }

    @Inject
    public UserUtil(Application application) {
        this.authPreferences = PreferenceManager.getDefaultSharedPreferences(application);
    }

    public void setToken(String token) {
        authPreferences.edit().putString(TOKEN, token).apply();
        String [] split  = token.split(" ");
        JWT jwt = new JWT(split[1]);
        String type = jwt.getClaim("type").asString();
        authPreferences.edit().putString(USER_TYPE,type).apply();
    }

    public String getUserType(){
        return authPreferences.getString(USER_TYPE,null);
    }

    @Nullable
    private String getUserCookie() {
        return authPreferences.getString(TOKEN, null);
    }
}
