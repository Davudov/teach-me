package com.davud.teachme.utils;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(int position, View view);
}
