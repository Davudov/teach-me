package com.davud.teachme.utils;

public class Constants {
    public static String URL = "https://teachmeback.herokuapp.com/";
    public static String TYPE_TEACHER = "Teacher";
    public static String TYPE_STUDENT = "Student";
}
