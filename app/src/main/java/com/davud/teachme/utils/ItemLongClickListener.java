package com.davud.teachme.utils;

import android.view.View;

public interface ItemLongClickListener {
    boolean onItemLongClick(int position, View view);
}