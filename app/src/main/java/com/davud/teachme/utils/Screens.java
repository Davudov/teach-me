package com.davud.teachme.utils;

public class Screens {
    public static final String SEARCH = "SEARCH";
    public static final String SAVED = "SAVED";
    public static final String PROFILE_STUDENT = "PROFILE_STUDENT";
    public static final String PROFILE_TEACHER = "PROFILE_TEACHER";
}
