package com.davud.teachme.presentation.student_profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.teachme.controllers.ProfileController;
import com.davud.teachme.entity.Profile;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class StudentProfilePresenter extends MvpPresenter<StudentProfileView> {
    private final ProfileController profileController;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final LocalProfile localProfile;
    private final LocalLessons localLessons;

    public StudentProfilePresenter(ProfileController profileController, LocalProfile localProfile, LocalLessons localLessons) {
        this.profileController = profileController;
        this.localProfile = localProfile;
        this.localLessons = localLessons;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getProfileFromLocal();
    }


    private void getProfileFromLocal() {
        disposable.add(localProfile.getProfile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(profile -> getViewState().getProfile(profile), e -> getViewState().onProfileNotExists()));
    }

    private Completable updateLocalProfile(Profile profile) {
        return Completable.fromAction(() -> localProfile.saveProfile(profile))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Timber::e);

    }


    public void createProfile(Profile profile) {
        disposable.add(profileController.createProfile(profile)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t->getViewState().startProgress())
                .doAfterTerminate(()->getViewState().stopProgress())
                .flatMap(t->getProfile())
                .doOnSuccess(t->getViewState().getProfile(t))
                .flatMapCompletable(this::updateLocalProfile)
                .subscribe(()->getViewState().message("Profile created"), Timber::e));
    }


    private Single<Profile> getProfile() {
        return profileController.getProfile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .flatMap(Single::just)
                .doOnError(Timber::e);
    }

    public void updateProfile(Profile profile) {
        disposable.add(profileController.updateProfile(profile)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t -> getViewState().startProgress())
                .doAfterTerminate(() -> getViewState().stopProgress())
                .doOnSuccess(t->getViewState().message("Profile updated"))
                .subscribe(result -> updateLocalProfile(profile), Timber::e));
    }

    public void signOut(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().clear().apply();
        disposable.add(Completable.fromAction(localProfile::deleteProfile)
                .andThen(Completable.fromAction(localLessons::deleteLessons))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> Timber.d("Profile and lessons deleted"), Timber::e));
    }
}
