package com.davud.teachme.presentation.teacher_profile;

import com.arellomobile.mvp.MvpView;
import com.davud.teachme.entity.Profile;

public interface TeacherProfileView extends MvpView {
     void getProfile(Profile profile);
     void onProfileNotExists();
     void message(String t);
     void startProgress();
     void stopProgress();
}
