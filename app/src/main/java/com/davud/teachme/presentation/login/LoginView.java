package com.davud.teachme.presentation.login;

import com.arellomobile.mvp.MvpView;

public interface LoginView extends MvpView {
    void onSuccess();

    void onError(String message);

    void startProgress();

    void stopProgress();
}
