package com.davud.teachme.presentation.search;

import com.arellomobile.mvp.MvpView;
import com.davud.teachme.entity.Lesson;

import java.util.List;

public interface SearchLessonsView extends MvpView {
    void onReadyData(List<Lesson> lessons);
    void showLoader();
    void hideLoader();
}
