package com.davud.teachme.presentation.search;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.teachme.controllers.LessonsController;
import com.davud.teachme.entity.Lesson;
import com.davud.teachme.persistance.lessons.LocalLessons;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class SearchPresenter extends MvpPresenter<SearchLessonsView> {
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final LocalLessons localLessons;
    private final LessonsController lessonsController;

    public SearchPresenter(LocalLessons localLessons, LessonsController lessonsController) {
        this.localLessons = localLessons;
        this.lessonsController = lessonsController;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getLessonsLocal();
    }

    public void getLessons() {
        disposable.add( lessonsController.getLessons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t -> getViewState().showLoader())
                .doAfterTerminate(() -> getViewState().hideLoader())
                .doOnSuccess(result -> getViewState().onReadyData(result))
                .doOnError(Timber::e)
                .flatMap(this::saveLessons)
                .subscribe());
    }

    private Single<String> saveLessons(List<Lesson> lessons) {
        return new Single<String>() {
            @Override
            protected void subscribeActual(SingleObserver<? super String> observer) {
                localLessons.updateLessons(lessons);
                observer.onSuccess("Lessons updated");
            }
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Timber::e)
                .doOnSuccess(Timber::d);
    }

    private void getLessonsLocal() {
        disposable.add(localLessons.getLessons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(t -> getViewState().onReadyData(t), Timber::e));
    }
}
