package com.davud.teachme.presentation.register;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.SignUpController;
import com.davud.teachme.entity.AuthResult;
import com.davud.teachme.entity.User;
import com.davud.teachme.utils.UserUtil;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RegisterPresenter extends MvpPresenter<RegisterView> {
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final UserUtil userUtil;
    private final SignUpController signUpController;
    private final ErrorController errorController;

    public RegisterPresenter(UserUtil userUtil, SignUpController signUpController, ErrorController errorController, Context context) {
        this.userUtil = userUtil;
        this.signUpController = signUpController;
        this.errorController = errorController;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void signUp(User user) {
        disposable.add(signUpController.signUp(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t -> getViewState().startProgress())
                .doAfterTerminate(() -> getViewState().stopProgress())
                .map(AuthResult::getToken)
                .subscribe(token -> {
                    userUtil.setToken(token);
                    getViewState().onSuccess();
                }, e -> getViewState().onError(errorController.convertBody(e).getMessage())));
    }
}
