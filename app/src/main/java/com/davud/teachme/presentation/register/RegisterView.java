package com.davud.teachme.presentation.register;

import com.arellomobile.mvp.MvpView;

public interface RegisterView extends MvpView {
    void startProgress();

    void stopProgress();

    void onSuccess();

    void onError(String message);
}
