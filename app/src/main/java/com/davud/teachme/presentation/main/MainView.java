package com.davud.teachme.presentation.main;

import com.arellomobile.mvp.MvpView;

public interface MainView extends MvpView {
    void onTeacherType();
    void onStudentType();
    void startProgress();
    void stopProgress();
    void getProfileType(String userType);
}
