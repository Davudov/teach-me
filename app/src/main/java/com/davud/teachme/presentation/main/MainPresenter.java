package com.davud.teachme.presentation.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.teachme.controllers.LessonsController;
import com.davud.teachme.controllers.ProfileController;
import com.davud.teachme.entity.Lesson;
import com.davud.teachme.entity.Profile;
import com.davud.teachme.persistance.lessons.LocalLessons;
import com.davud.teachme.persistance.profile.LocalProfile;
import com.davud.teachme.utils.Constants;
import com.davud.teachme.utils.UserUtil;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private final ProfileController profileController;
    private final LocalProfile localProfile;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final LocalLessons localLessons;
    private final LessonsController lessonsController;
    private final String typeUser;

    public MainPresenter(ProfileController profileController, LocalProfile localProfile, UserUtil userUtil, LocalLessons localLessons, LessonsController lessonsController) {
        this.profileController = profileController;
        this.localProfile = localProfile;
        this.localLessons = localLessons;
        this.lessonsController = lessonsController;
        typeUser = userUtil.getUserType();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getProfile();
    }

    private Single<String> getLessons() { //Requests for the lessons if type is 'Student'
        return lessonsController.getLessons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .flatMap(this::saveLessons)
                .doOnError(Timber::e)
                .flatMap(Single::just);
    }

    private Single<String> saveLessons(List<Lesson> lessons) {
        return new Single<String>() {
            @Override
            protected void subscribeActual(SingleObserver<? super String> observer) {
                localLessons.updateLessons(lessons);
                observer.onSuccess("Lessons updated");
            }
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Timber::e);
    }


    private void getProfile() {
        disposable.add(profileController.getProfile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t -> getViewState().startProgress())
                .doAfterTerminate(() -> {
                    if (typeUser.equals(Constants.TYPE_TEACHER)) getViewState().onTeacherType();
                    if (typeUser.equals(Constants.TYPE_STUDENT)) getViewState().onStudentType();
                    getViewState().stopProgress();
                })
                .flatMap(this::saveProfile)
                .filter(t -> typeUser.equals(Constants.TYPE_STUDENT))
                .toSingle()
                .flatMap(t->getLessons())
                .subscribe(t ->Timber.d(t), Timber::e));

    }

    private Single<String> saveProfile(Profile profile) {
        return new Single<String>() {
            @Override
            protected void subscribeActual(SingleObserver<? super String> observer) {
                localProfile.updateProfile(profile);
                observer.onSuccess("Profile updated");
            }
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(Timber::e)
                .doOnSuccess(Timber::d);
    }


}
