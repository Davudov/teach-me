package com.davud.teachme.presentation.login;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.teachme.controllers.ErrorController;
import com.davud.teachme.controllers.LoginController;
import com.davud.teachme.entity.AuthResult;
import com.davud.teachme.entity.User;
import com.davud.teachme.utils.UserUtil;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView> {
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final LoginController loginController;
    private final UserUtil userUtil;
    private final ErrorController errorController;

    public LoginPresenter(LoginController loginController, UserUtil userUtil, ErrorController errorController) {
        this.loginController = loginController;
        this.userUtil = userUtil;
        this.errorController = errorController;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

    }


    public void doLogin(User user) {
        disposable.add(loginController.doLogin(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(t -> getViewState().startProgress())
                .doAfterTerminate(() -> getViewState().stopProgress())
                .map(AuthResult::getToken)
                .subscribe(token -> {
                    userUtil.setToken(token);
                    getViewState().onSuccess();
                }, e -> getViewState().onError(errorController.convertBody(e).getMessage())));
    }

}
